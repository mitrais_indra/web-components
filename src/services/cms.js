import axios from 'axios';
import q from 'q';

import config from '@/data/config.json';
import endpointConfig from './endpoint';


const CMSConfig = {
  endpoint: endpointConfig.endpoints.cms.endpoint,
  version: 1,
  key: config.cms.key
};

const baseUrl = CMSConfig.endpoint + '/api/v' + CMSConfig.version + '/';
// The auth token name which is used as the key value when persisting the session token
const token = 'Token token=' + CMSConfig.key;
const customConfig = {
  baseURL: baseUrl,
  timeout: 10000,
  headers: {
    'Authorization': `${token}`,
    'Content-Type': 'application/json'
  }
};
let cmsProvider = axios.create(customConfig);

cmsProvider.interceptors.response.use(function (response) {
  // Get response data
  return response.data;
}, function (error) {
  // Do something with response error
  return Promise.reject(error);
});

function getDirectoriesByLocationId (pagingParams, id) {
  // CMSConfig.endpoint + '/api/v1/locations/'+ id +'/directories.json'
  const parentEntity = 'locations';
  const entity = 'directories';
  return getListWithIdPaged(pagingParams, parentEntity, id, entity);
}

function getSingleDirectoryByLocationId (locationId, directoryId) {
  // CMSConfig.endpoint + '/api/v1/locations/'+ locationId +'/directories/' + directoryId + '.json'
  const parentEntity = 'locations';
  const entity = 'directories';
  return getSingleWithId(parentEntity, locationId, entity, directoryId);
}

function getWebProfile (id) {
  // CMSConfig.cms.endpoint + '/api/v1/locations/'+ id +'/web_profile.json'
  const parentEntity = 'locations';
  const entity = 'web_profile';
  return getSingleWithId(parentEntity, id, entity);
}

function getMessagesByLocationId (pagingParams, id) {
  // CMSConfig.cms.endpoint + '/api/v1/locations/'+ id +'/messages.json'
  const parentEntity = 'locations';
  const entity = 'messages';
  return getListWithIdPaged(pagingParams, parentEntity, id, entity);
}

function getMessageItemWithID (locationId, id) {
  // CMSConfig.endpoint + '/api/v1/locations/'+ locationId +'/messages/' + id + '.json'
  const parentEntity = 'locations';
  const entity = 'messages';
  return getSingleWithId(parentEntity, locationId, entity, id);
}

function getSingleWithId (parentEntity, parentId, entity, entityId) {
  let deferred = q.defer();
  if (entityId) {
    cmsProvider.get(`${parentEntity}/${parentId}/${entity}/${entityId}`)
      .then(function (data) {
        deferred.resolve(data);
      }).catch(function (error) {
        deferred.reject(error);
      });
  } else if (entity && !entityId) {
    cmsProvider.get(`${parentEntity}/${parentId}/${entity}`)
      .then(function (data) {
        deferred.resolve(data);
      }).catch(function (error) {
        deferred.reject(error);
      });
  } else {
    cmsProvider.get(`${parentEntity}/${parentId}`)
      .then(function (data) {
        deferred.resolve(data);
      }).catch(function (error) {
        deferred.reject(error);
      });
  }

  return deferred.promise;
}

function getListWithIdPaged (pagingParams, parentEntity, id, entity) {
  let deferred = q.defer();
  if (entity) {
    // CMSConfig.endpoint + '/api/v1/locations/'+ id +'/directories.json'
    cmsProvider.get(`${parentEntity}/${id}/${entity}`, {
      params: pagingParams
    }).then(function (data) {
      deferred.resolve(data);
    }).catch(function (error) {
      deferred.reject(error);
    });
  } else {
    cmsProvider.get(`${parentEntity}/${id}`, {
      params: pagingParams
    }).then(function (data) {
      deferred.resolve(data);
    }).catch(function (error) {
      deferred.reject(error);
    });
  }

  return deferred.promise;
}

export default {
  getDirectoriesByLocationId,
  getSingleDirectoryByLocationId,
  getWebProfile,
  getMessagesByLocationId,
  getMessageItemWithID
};
