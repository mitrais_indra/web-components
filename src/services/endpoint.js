const endpoints = {
  'um': {
    'endpoint': 'https://um-staging.xperior.com',
    'version': 1,
    'status': {
      'label': 'User Management',
      'request': '/ping'
    }
  },
  'cms': {
    'endpoint': 'https://cms-staging.platform.im',
    'status': {
      'label': 'CMS',
      'request': '/ping'
    }
  },
  'sc': {
    'endpoint': 'https://sc-staging.xperior.com',
    'version': 1,
    'status': {
      'label': 'Services',
      'request': '/ping'
    }
  },
  'pm': {
    'endpoint': 'https://pm-staging.xperior.com',
    'version': 1,
    'status': {
      'label': 'Property',
      'request': '/ping'
    }
  },
  'imgix': {
    'endpoint': 'https://cms-xperior.imgix.net',
    'version': '1',
    'status': {
      'label': 'Image CDN',
      'request': '/ping'
    }
  }
};

export default {
  endpoints
};