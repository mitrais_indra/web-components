import Vue from 'vue'
// import App from './App.vue'
// import router from './router'
// import store from './store'
import wrap from '@vue/web-component-wrapper';

import OffersDashboard from '@/components/OffersDashboard/index.vue';

// Vue.config.productionTip = false

// new Vue({
//   router,
//   store,
//   render: h => h(App)
// }).$mount('#app')

const OffersDashboardElement = wrap(Vue, OffersDashboard);
window.customElements.define('offer-block', OffersDashboardElement);
