function slug(value) {
  if (!value) return value;
  let result = value.toString()
            .toLowerCase()
            .trim()
            .replace(/&/g, '-and-') //
            .replace(/[\s\W-]+/g, '-')
            .replace(/--+/g, '-')
            .replace(/^-+|-+$/g, '');
  return result;
}

export default slug;
