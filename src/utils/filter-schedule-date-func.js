import moment from 'moment';

function filterByScheduleDateFunc (items) {
  var nowMoment = moment();
  var messageItems = [];
  _.remove(items, (item) => {
    var start = moment(item.schedule.start_date).add(item.schedule.start_time, 'hours');
    var end = moment(item.schedule.end_date).add(item.schedule.end_time, 'hours');
    var isBetween = nowMoment.isBetween(start, end);
    var isStartDay = nowMoment.isSame(start, 'day');
    var isEndDay = nowMoment.isSame(end, 'day');

    // we should have a schedule so remove if we dont
    if (item.schedule.start_date === null || item.schedule.end_date === null) {
      return true;
    }

    // check within start and end date
    if (isBetween === true || isStartDay === true || isEndDay === true) {
      return false;
    } else {
      return true;
    }
  });
  if (items.length) {
    messageItems = items;
  }
  return messageItems;
}

export default filterByScheduleDateFunc;
