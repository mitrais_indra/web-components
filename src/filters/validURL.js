const filter = function(url) {
  if (!/^(?:f|ht)tps?:\/\//.test(url)) {
    url = 'https://' + url;
  }

  return url;
};

export default filter;