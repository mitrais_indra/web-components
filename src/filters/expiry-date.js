import moment from 'moment';

function checkPlural (diff) {
  if (diff > 1) {
    return 's';
  } else {
    return '';
  }
}

export default function (input) {
  if (typeof (input) === 'undefined' || input === null) {
    return input;
  }
  let ends = moment(input.end_date).add(input.end_time, 'hours');
  let parsedInput = '';
  let hourDiff = ends.diff(moment(), 'hours');
  let dayDiff = ends.diff(moment(), 'days');
  let weekDiff = ends.diff(moment(), 'weeks');
  let monthDiff = ends.diff(moment(), 'months');
  let yearDiff = ends.diff(moment(), 'years');
  if (hourDiff < 24) {
    parsedInput = 'Expiring in ' + hourDiff + ' hour' + checkPlural(hourDiff);
  } else if (dayDiff < 7) {
    parsedInput = 'Expiring in ' + dayDiff + ' day' + checkPlural(dayDiff);
  } else if (dayDiff >= 7 && dayDiff < 31) {
    parsedInput = 'Expiring in ' + weekDiff + ' week' + checkPlural(weekDiff);
  } else if (dayDiff >= 31 && dayDiff < 365) {
    parsedInput = 'Expiring in ' + monthDiff + ' month' + checkPlural(monthDiff);
  } else if (dayDiff >= 365) {
    parsedInput = 'Expiring in ' + yearDiff + ' year' + checkPlural(yearDiff);
  }
  return parsedInput;
}
