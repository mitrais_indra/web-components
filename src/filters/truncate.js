const filter = function(text, length, clamp) {
  if (typeof (text) === 'undefined' || text === null || text.length <= length) {
    return text;
  }
  let slicedText = text.substr(0, length);
  let slicedTextFullWords = slicedText.substr(0, Math.min(slicedText.length, slicedText.lastIndexOf(' ')));
  return slicedTextFullWords + (clamp ? clamp : '...'); // + slicedTextFullWords.length;
};

export default filter;