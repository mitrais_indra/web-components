import CMSService from '@/services/cms';
import config from '@/data/config.json';
import filterByScheduleDateFunc from '@/utils/filter-schedule-date-func';

import slug from '@/utils/slug';
import _ from 'lodash';


export default {
  name: 'dashboard-offers',
  data () {
    return {
      offerWidgetItems: [],
      totalItems: 0
    };
  },
  created() {
    let vm = this;
    vm.getOffersWidgetData();
  },
  methods: {
    slug(value) {
      return slug(value);
    },
    getCategory(subType) {
      switch(subType) {
        default: return subType;
      }
    },
    getOffersWidgetData () {
      let vm = this;
      let offers = [];
      
      // find the cms for the category
      let item = _.find(config.routes.offers.routes, { 'route': 'all' });

      let pagingParams = {
        page: 0,
        per_page: item.cms[0].per_page,
        sort_by: item.cms[0].sort_by,
        message_types: item.cms[0].message_types,
        sort_order: item.cms[0].sort_order ? item.cms[0].sort_order : null,
        published: item.cms[0].published ? item.cms[0].published : null
      };
      // console.log('offers-index', item.title, pagingParams);
      CMSService.getMessagesByLocationId(pagingParams, config.cms.id).then(function (response) {
        offers = response.messages;
        vm.totalItems = response.meta.message_count;
        vm.offerWidgetItems = filterByScheduleDateFunc(offers).slice(0, 4);
        // console.log('offer items: ', vm.offerWidgetItems);
      }).catch(error => console.debug(error));
    }
  }
};
